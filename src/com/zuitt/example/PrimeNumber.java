package com.zuitt.example;


import java.util.Arrays;
import java.util.HashMap;

public class PrimeNumber {
    public static void main(String[] args) {

        int[] primeNumbers = new int[5];

        primeNumbers[0] = 2;
        primeNumbers[1] = 3;
        primeNumbers[2] = 5;
        primeNumbers[3] = 7;
        primeNumbers[4] = 11;

        System.out.println("The first prime number is: " + primeNumbers[0]);


        String[] friends = {"John", "Jane", "Chloe", "Zoey"};
        System.out.println("My friends are: " + Arrays.toString(friends));

        HashMap<String, Integer> stocks = new HashMap<String, Integer>(){
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };

        System.out.println("Our current inventory consists of: " + stocks);
    }
}
